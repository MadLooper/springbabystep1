package UsersRepo;

/**
 * Created by KW on 10/12/2017.
 */
public class User {
    String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
