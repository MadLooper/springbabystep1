package UsersRepo;

/**
 * Created by KW on 10/12/2017.
 */
public interface Logger {
    void logg(String message);
    void setName(String name);
    void setVersion (int version);
}
