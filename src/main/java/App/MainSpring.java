package App;

import UsersRepo.User;
import UsersRepo.UsersRepository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by KW on 10/13/2017.
 */
public class MainSpring {
    public static void main(String[] args) {
        //tworzymy kontekst, ktory wykorzystuje wszystkie ustawienia
        ApplicationContext context=new ClassPathXmlApplicationContext("configuration.xml");
       //wyciagamy beana.Podajemy nazwe i klase, jaki jest wykorzystany
        UsersRepository repozytoriumUzytkownikow = context.getBean("repozytoriumUzytkownikow", UsersRepository.class);
        User Adam = repozytoriumUzytkownikow.createUser("Adam");

    }
}
//Tworzymy user,
//dependency injection- Spring dochodzi do tego w jakich miejscach wstrzykuje sie rozne komponenty.
