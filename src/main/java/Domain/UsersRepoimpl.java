package Domain;

import UsersRepo.Logger;
import UsersRepo.User;
import UsersRepo.UsersRepository;

/**
 * Created by KW on 10/12/2017.
 */
public class UsersRepoimpl implements UsersRepository {
    private Logger logger;

    public UsersRepoimpl(Logger logger,String localization, String dbName) {
        this.logger = logger;
        logger.logg("Lokalizacja repo: "+localization+"/"+dbName);
    }

    public User createUser(String name) {
        logger.logg("Tworzenie uzytkownika " + name);
        return new User(name);
    }
}
//podczas tworzenia beanow, spring zaglada tutaj - widzi konstruktor,wywoluje go
//tworzy obiekt danej klasy, nastepnie wstrzykuje wlasciwosci z np. z xmla
