package Domain;

import UsersRepo.Logger;

import java.util.Date;

/**
 * Created by KW on 10/12/2017.
 */
public class Loggerimpl implements Logger {
private String name;
private int version;
    public void logg(String message) {
        System.out.println(new Date()+": "+"["+name+"]"+"v."+"["+version+"]"+ message);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
